﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatter
{
    public class ChannelModel
    {
        public ChannelModel()
        {
            Users = new List<UserModel>();
            Messages = new List<string>();
        }

        public string Name { get; set; }
        public List<UserModel> Users { get; set; }
        public List<string> Messages { get; set; }
    }
}
