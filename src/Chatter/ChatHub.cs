﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatter
{
    public class ChatHub : Hub
    {
        public static List<ChannelModel> Channels = new List<ChannelModel>();
        public static List<UserModel> AllUsers = new List<UserModel>();

        public override Task OnConnected()
        {
            StoreUser();
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            RemoveUser();
            return base.OnDisconnected(stopCalled);
        }

        private UserModel GetUser()
        {
            var connectedUser = AllUsers.First(m => m.ConnectionId == Context.ConnectionId);
            return connectedUser;
        }

        private void RemoveUser()
        {
            AllUsers.Remove(GetUser());
        }

        private void StoreUser()
        {
            AllUsers.Add(new UserModel() { ConnectionId = Context.ConnectionId });
        }

        private void CreateChannelIfNotExist(string channel)
        {
            var channelExists = Channels.Any(m => m.Name == channel);
            if (!channelExists) { Channels.Add(new ChannelModel() { Name = channel }); }
        }

        private ChannelModel GetChannel(string channel)
        {
            var ch = Channels.First(m => m.Name == channel);
            return ch;
        }

        private void AddUserToChannel(string user, string channel)
        {
            var ch = GetChannel(channel);
            ch.Users.Add(GetUser());
        }

        private void RemoveUserFromChannel(string user, string channel)
        {
            var ch = GetChannel(channel);
            ch.Users.Remove(GetUser());
        }

        public bool RegisterUser(string user)
        {
            if (AllUsers.Any(m => m.Name == user))
            {
                return false;
            }
            else
            {
                var connectedUser = GetUser();
                connectedUser.Name = user;
                return true;
            }
        }

        public void Connect(string newUser, string channel)
        {
            CreateChannelIfNotExist(channel);
            Groups.Add(Context.ConnectionId, channel);
            AddUserToChannel(newUser, channel);
            Clients.Caller.getConnectedUsers(GetChannel(channel).Users.Select(m => m.Name).ToList(), channel);
            Clients.OthersInGroup(channel).newUserAdded(newUser, channel);
        }

        public void Disconnect(string user, string channel)
        {
            RemoveUserFromChannel(user, channel);
            Clients.Group(channel).userRemoved(user, channel);
        }

        public void Send(string originatorUser, string message, string channel)
        {
            Clients.Group(channel).messageReceived(originatorUser, message, channel);
        }
    }
}
