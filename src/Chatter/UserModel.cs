﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Chatter
{
    public class UserModel
    {
        public string ConnectionId { get; set; }
        public string Name { get; set; }
    }
}
