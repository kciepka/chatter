﻿var app = angular.module("chatterApp", []);

var controller = app.controller("chatterController", function ($scope) {
    var chat = $.connection.chatHub;
    $.connection.hub.logging = false;
    $scope.userName = "";
    $scope.userMessage = "";
    $scope.newChannel = "";
    $scope.channels = [];
    $scope.channels.push({ 'name': 'general', 'messages': [], 'users': [], 'unread' : 0 });
    $scope.currentChannelId = 0;

    $('#messageInput').keypress(function (e) {
        if (e.which == 13) {
            $scope.sendMessage();
            console.log("ENTER presssed. sending message: ", $scope.userMessage);
        }
    });

    $('#userNameInput').keypress(function (e) {
        if (e.which == 13) {
            $scope.setUserName();
            console.log("ENTER presssed. setting user name: ", $scope.userName);
        }
    });

    $('#newChannelInput').keypress(function (e) {
        if (e.which == 13) {
            $scope.joinChannel($('#newChannelInput').val());
            console.log("ENTER presssed. joining new channel: ", $scope.channels[$scope.currentChannelId]);
        }
    });

    $(window).bind('beforeunload', function () {
        console.log("beforeunload");
        for(i=0; i<$scope.channels.length; i++){
            chat.server.disconnect($scope.userName, $scope.channels[i].name);
            console.log("disconecting user from channel: ", $scope.channels[i].name);
        }
      //  return false;
    });

    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > 70) {
            $('.ui.sticky').sticky({
                context: "#context"
            });
        }
    });

    $scope.setUserName = function () {
        $scope.nameExists = false;
        $scope.nameTooLong = false;
        if ($scope.userName != "") {
            if ($scope.userName.length > 15) {
                $scope.nameTooLong = true;
                $scope.$apply();
                return;
            }
            $scope.nameTooLong = false;
            $.connection.hub.start().done(function () {
                console.log("registering user...");
                chat.server.registerUser($scope.userName).done(function (result) {
                    if (result) {
                        $scope.nameExists = false;
                        console.log("user registered successfully");
                        chat.server.connect($scope.userName, $scope.channels[$scope.currentChannelId].name);
                        console.log("starting connection hub: ", $scope.userName, $scope.channels[$scope.currentChannelId].name);
                        $('#userNameModal').modal('hide');
                        $('#messageInput').focus();
                    }
                    else {
                        console.log("username already exists");
                        $scope.nameExists = true;
                        $scope.$apply();
                    }
                });
            });
        }
    }

    chat.client.getConnectedUsers = function (userList, ch) {
        console.log("getting connected users for channel: "+ch, userList);
        for (var i = 0; i < $scope.channels.length; i++) {
            if ($scope.channels[i].name == ch) {
                $scope.channels[i].users = userList;
                break;
            }
        }
        $scope.$apply();
    };

    chat.client.newUserAdded = function (newUser, channel) {
        console.log("new user added: " + newUser + " channel: " + channel);
        addUser(newUser, channel);
    }

    chat.client.userRemoved = function (user, channel) {
        console.log("user removed: " + user + " channel: " + channel);
        removeUser(user, channel);
    }

    function addUser(user, ch) {
        for (i = 0; i < $scope.channels.length; i++) {
            if ($scope.channels[i].name == ch) {
                console.log("adding...");
                $scope.channels[i].users.push(user);
                break;
            }
        }
        $scope.$apply();
    }

    function removeUser(user, ch) {
        for (i = 0; i < $scope.channels.length; i++) {
            if ($scope.channels[i].name == ch) {
                console.log("removing...");
                $scope.channels[i].users.splice($scope.channels[i].users.indexOf(user), 1);
                break;
            }
        }
        $scope.$apply();
    }

    $scope.sendMessage = function (message) {
        if ($('#messageInput').val() != '') {
            $scope.userMessage = $("#messageInput").val();
            console.log("sending message...");
            chat.server.send($scope.userName, $("#messageInput").val(), $scope.channels[$scope.currentChannelId].name);
            $('#messageInput').val('');
            $('#messageInput').focus();
        }
    }

    $scope.changeChannel = function (ch) {
        for (i = 0; i < $scope.channels.length; i++) {
            if ($scope.channels[i].name == ch) {
                $scope.currentChannelId = i;
                $scope.channels[i].unread = 0;
                $('#messageInput').focus();
                console.log("channel changed to: ", ch);
                break;
            }
        }
    }

    $scope.joinChannel = function (ch) {
        $scope.channelExists = false;
        $scope.channelTooLong = false;
        if (ch != "") {
            if (ch.length > 15) {
                $scope.channelTooLong = true;
                $scope.$apply();
                return;
            }
            for (i = 0; i < $scope.channels.length;i++){
                if ($scope.channels[i].name == ch) {
                    $scope.channelExists = true;
                    $scope.$apply();
                    return;
                }
            }
            $scope.channelTooLong = false;
            $scope.channelExists = false;
            $scope.channels.push({ 'name': ch, 'messages': [], 'users': [] });
            chat.server.connect($scope.userName, ch);
            $scope.changeChannel(ch);
            $('#newChannelModal').modal('hide');
            $('#newChannelInput').val('');
            $scope.newChannel = "";
            console.log("new channel created: ", ch);
        }
        else {
            $('#newChannelModal').modal('hide');
            $('#newChannelInput').val('');
            $scope.newChannel = "";
        }
    }

    $scope.leaveChannel = function (ch) {
        for (i = 0; i < $scope.channels.length; i++) {
            if ($scope.channels[i].name == ch) {
                $scope.channels.splice(i, 1);
                chat.server.disconnect($scope.userName, ch);
                console.log("channel left: ", ch);
                break;
            }
        }
    }

    $scope.showNewChannelModal = function () {
        $('#newChannelModal').modal({ closable: true }).modal('show');
        $('#newChannelInput').focus();
    }

    chat.client.messageReceived = function (userName, message, channel) {
        console.log("message received user: " + userName + " message: " + message + " channel: " + channel);
        for (i = 0; i < $scope.channels.length; i++) {
            if ($scope.channels[i].name == channel) {
                $scope.channels[i].messages.unshift({ 'userName': userName, 'message': message });
                if (i != $scope.currentChannelId && userName != $scope.userName) {
                    $scope.channels[i].unread += 1;
                }
                break;
            }
        }
        $scope.$apply();
    };
});

$('#userNameModal').modal({ closable: false }).modal('show');
